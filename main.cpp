#include <iostream>
#include <conio.h>
#include <malloc.h>
#include <stdio.h>

using namespace std;

//Linked list Structure

typedef struct node{
    int data; //will store data
    node *next; //the reference to the next node
};
    typedef struct node node;
    node *start = NULL;
    void display();
    void insertend();
    void insertbeg();
    void delend();
    void delbeg();
    void insertmid();
    void delmid();
    void modify();

    int main(){
        int a;
        //clrscre();
        cout<< "THIS PROGRAM GIVES YOU THE SINGLE LINKLIST\n";
        do{
            cout<< "Enter Your Choice:\n";
            cout<< "1.INSERT element at the END of LinkList\n";
            cout<< "2.INSERT element at the BEGINING of LinkList\n3.Delete from";
            cout<< "END\n4.DELETE from BEGINING\n5.INSERT at MIDDLE\n";
            cout<< "6.DELETE from MIDDLE\n7.MODIFY any element\n8.EXIT\n";
            fflush(stdin);
            cin>>a;
            switch(a){
            case 1:
                insertend();
                display();
                break;
            case 2:
                insertbeg();
                display();
                break;
            case 3:
                delend();
                display();
                break;
            case 4:
                delbeg();
                display();
                break;
            case 5:
                insertmid();
                display();
                break;
            case 6:
                delmid();
                display();
                break;
            case 7:
                modify();
                display();
            case 8:
                exit(0);
           }
        }
        while(a!=8);
        getch();
    }
    void insertend(){
        node *p,*q;
        int item;
        cout<< "Enter Your Elements In The Stack\n";
        cin>> item;
        p=(node *)malloc(sizeof(node));
        p->data=item;
        p->next=NULL;
        if(start==NULL){
            start=p;
        }else{
            q=start;
            while(q->next!=NULL){
                q=q->next;
            }
            q=q->next;
        }
    }
    void display(){
        node *temp;
        temp=start;
        cout<< "The Link List is As Follows";
        while(temp->next!=NULL){
            cout<<temp->data;
            temp=temp->next;
        }
        cout<<temp->data;
    }
    void delend(){
        node *p,*q,*k;
        q=start;
        if(start->data==0){
            //if we write here if(start==NULL) then it wil not print
            cout<< "THERE IS NO EMENT IN THE LIST\n";
            //last value and remains it zero
        }else if(start->next==NULL){
            k=start;
            start=NULL;
            free(k);
        }else{
            while(q->next->next!=NULL){
                q=q->next;
            }
            p=q->next->next;
            q->next=NULL;
            free(p);
        }
    }
    void insertbeg(){
        int item;
        node *p,*q;
        cout<< "Enter the value which you want to insert at the beginning\n";
        cin>> item;
        p=start;
        q=(node *)malloc(sizeof(node));
        q->data=item;
        q->next=p;
        start=q;
    }
    void delbeg(){
        if(start==NULL){
            cout<< "THERE IS NO ELEMENT IN THE LIST\n";
        }
        node *p;
        p=start;
        start=p->next;
        free(p);
    }
    void insertmid(){
        int item1,item2;
        node *p,*q,*k;
        cout<< "ENTER THE PREVIOUS VALUE AFTER WHICH YOU WANT TO INSERT A NEW ELEMENT\n";
        cin>>item1;
        cout<< "ENTER THE VALUE OF NEW NODE\n";
        cin>> item2;
        q=(node *)malloc(sizeof(node));
        q->data=item2;
        q->next=NULL;
        p=start;
        while(p->data!=item1){
            p=p->next;
        }
        k=p->next;
        p->next=q;
        q->next=k;
    }
    void delmid(){
        int item;
        node *p,*q,*k;
        cout<< "ENTER THE VALUE OF THAT VALUE WHICH YOU WANT TO DELETE\n";
        cin>>item;
        p=start;
        while(p->data!=item){
            p=p->next;
        }
        q=p->next->next;
        k=p->next;
        p->next=q;
        free(k);
    }
    void modify(){
        int item1,item2;
        node *p,*q;
        cout<< "Enter the value you want to modify\n";
        cin>> item1;
        cout<< "Enter the new value\n";
        cin>>item2;
        p=start;
        while(p->data!=item1){
            p=p->next;
        }
        p->data=item2;
    }
